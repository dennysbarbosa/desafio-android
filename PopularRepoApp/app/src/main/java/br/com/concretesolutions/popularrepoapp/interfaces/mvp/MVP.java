package br.com.concretesolutions.popularrepoapp.interfaces.mvp;

import java.util.List;

import br.com.concretesolutions.popularrepoapp.entity.ItemRepositoryEntity;
import br.com.concretesolutions.popularrepoapp.entity.PullRequestEntity;
import br.com.concretesolutions.popularrepoapp.entity.RepositoryEntity;

/**
 * Created by Dennys on 08/11/2017.
 */

public interface MVP {

    interface HomeView {

        void showProgressBar();
        void hideProgressBar();
        List<ItemRepositoryEntity> getItens();
        void loadRecycleViewRepository();
        void showMessage();
    }

    interface HomePresenter{

        void showProgressBar();
        void hideProgressBar();
        void findRepositoryByPage(int page);
        void showMessage();
        void getRepositoryByPage(RepositoryEntity repositoryEntity);
        List<ItemRepositoryEntity> getItemsToBeLoaded(int start, int end);
        boolean loadMoreItens();
    }

    interface HomeModel{

        void getRepositoryByPage(int page);
    }


    interface PullRequestRepoView {

        void showProgressBar();
        void hideProgressBar();
        void showMessage();
        void loadViewsPullRequestRepo(List<PullRequestEntity> listPullRequestRepo, int opened, int closed);

    }

    interface PullRequestRepoPresenter {

        void showProgressBar();
        void hideProgressBar();
        void showMessage();
        void getItensPullRequestRepo(List<PullRequestEntity> listPullRequestRepo);
        void findPullResquestRepoByCreatorRepo(String creator, String repository);
    }

    interface PullRequestRepoModel {

        void getPullRequestByCreatorRepo(String creator, String repository);
    }
}
