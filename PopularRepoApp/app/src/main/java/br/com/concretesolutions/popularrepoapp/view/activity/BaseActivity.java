package br.com.concretesolutions.popularrepoapp.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ProgressBar;
import android.widget.Toast;

import br.com.concretesolutions.popularrepoapp.Application;
import br.com.concretesolutions.popularrepoapp.R;
import butterknife.BindView;

/**
 * Created by Dennys on 11/11/2017.
 */

public class BaseActivity extends AppCompatActivity {

    public Application application;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        application = ((Application) getApplication());
    }

    public void showMessage(){

        Toast.makeText(this, getString(R.string.message_erro), Toast.LENGTH_LONG).show();
    }
}
