package br.com.concretesolutions.popularrepoapp.interfaces.delegator;

/**
 * Created by Dennys on 08/11/2017.
 */

public interface AsyncTaskDelegate {

    void processStart();
    void processFinish(Object object);
}
