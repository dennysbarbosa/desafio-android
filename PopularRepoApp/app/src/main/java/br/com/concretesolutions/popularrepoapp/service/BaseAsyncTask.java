package br.com.concretesolutions.popularrepoapp.service;

import android.os.AsyncTask;

import java.util.List;

import br.com.concretesolutions.popularrepoapp.BuildConfig;
import br.com.concretesolutions.popularrepoapp.entity.PullRequestEntity;
import br.com.concretesolutions.popularrepoapp.interfaces.api.ApiServices;
import br.com.concretesolutions.popularrepoapp.interfaces.delegator.AsyncTaskDelegate;
import br.com.concretesolutions.popularrepoapp.net.RetrofitConnection;

/**
 * Created by Dennys on 03/07/2017.
 */

public abstract class BaseAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    protected AsyncTaskDelegate asyncTaskDelegate;
    public BaseAsyncTask(){}

    protected ApiServices getApiServices() {

        return new RetrofitConnection().connection(BuildConfig.API_URL).create(ApiServices.class);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (asyncTaskDelegate != null) {
            asyncTaskDelegate.processStart();
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        asyncTaskDelegate.processFinish(null);
    }
}
