package br.com.concretesolutions.popularrepoapp.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;
import br.com.concretesolutions.popularrepoapp.R;
import br.com.concretesolutions.popularrepoapp.entity.PullRequestEntity;
import br.com.concretesolutions.popularrepoapp.view.activity.BaseActivity;
import br.com.concretesolutions.popularrepoapp.view.holder.PullRequestRepoViewHolder;

/**
 * Created by Dennys on 08/11/2017.
 */

public class PullRequestRepoRecycleViewAdapter extends RecyclerView.Adapter<PullRequestRepoViewHolder> {

    private List<PullRequestEntity> listPullRequestRepo;
    private Context context;

    public PullRequestRepoRecycleViewAdapter(List<PullRequestEntity> listPullRequestRepo) {
        this.listPullRequestRepo = listPullRequestRepo;
    }

    @Override
    public PullRequestRepoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        context = parent.getContext();
        return new PullRequestRepoViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pull_request_repos, null));
    }

    @Override
    public void onBindViewHolder(PullRequestRepoViewHolder holder, int position) {

        holder.txtViewPullRequestRepoName.setText(listPullRequestRepo.get(position).titlePullRequest);
        holder.txtViewPullRequestRepoUserName.setText(listPullRequestRepo.get(position).userEntity.login);
        ((BaseActivity) context).application.loadImageUrl(holder.imageViewPullRequestRepoAvatar, listPullRequestRepo.get(position).userEntity.avatarUrl);

        if(listPullRequestRepo.get(position).bodyPullRequest == null || listPullRequestRepo.get(position).bodyPullRequest.equals("")){
            holder.txtViewPullRequestRepoDescription.setVisibility(View.GONE);
        }
        holder.txtViewPullRequestRepoDescription.setText(listPullRequestRepo.get(position).bodyPullRequest);

    }

    @Override
    public int getItemCount() {
        return listPullRequestRepo != null ? listPullRequestRepo.size() : 0;
    }

}
